package net.e621.e6l

object Platform {
	interface FSAccess {
		companion object {
			interface LocalFile {
				val md5: MD5Checksum
			}
			data class MD5Checksum(val hexString: String)
		}
	}
	interface GUILibrary {
		fun initAndDrawMain(args: Array<String>)
	}
	interface JSONImplementation {
		fun parseDataModifiedResponse(jsonString: String): ESixAPI.DataModifiedResponse?
		fun parseFFDList(jsonString: String): List<ESixAPI.Posts.FFDData>?
		fun parseInternalPost(jsonString: String): ESixAPI.Posts.InternalPost?
		fun parseInternalPostList(jsonString: String): List<ESixAPI.Posts.InternalPost>?
		fun parseInternalTag(jsonString: String): ESixAPI.Tags.InternalTag?
		fun parseInternalTagAlias(jsonString: String): ESixAPI.Implications.InternalTagAlias?
		fun parseInternalTagList(jsonString: String): List<ESixAPI.Tags.InternalTag>?
		fun parsePostExistanceStatus(jsonString: String): ESixAPI.Posts.PostExistanceStatus?
		fun parsePostUploadResponse(jsonString: String): ESixAPI.Posts.PostUploadResponse?
		fun parseWikiPage(jsonString: String): ESixAPI.Wiki.WikiPage?
	}
	interface WebClient {
		companion object {
			private const val E6L_UA_FRAGMENT = "e6l-kotlin/0.1 (by YoshiRulz on e621, FurryPedant on GitLab)"
			data class ResponseBodyOrStatus(val status: Int, val body: String? = null)
			internal fun genUA(webUAFragment: String, compileTarget: String, osArch: String = System.getProperty("os.arch"), os: String = System.getProperty("os.name"), osVersion: String = System.getProperty("os.version"), ktVersion: KotlinVersion = KotlinVersion.CURRENT) = "$E6L_UA_FRAGMENT $webUAFragment ($osArch; $os/$osVersion; Kotlin/$compileTarget/$ktVersion)"
			fun paramsToQueryString(params: Map<String, Any>?) =
				params?.apply { "?" + entries.joinToString("&") { "${it.key}=${it.value}" } } ?: ""
		}
		fun withUA(compileTarget: String): WebClient {
			setUA(genUA(genUAFragment(), compileTarget))
			return this
		}
		fun genUAFragment(): String
		fun get(uri: String, params: Map<String, Any>? = null): ResponseBodyOrStatus
		fun post(uri: String, params: Map<String, Any>?): ResponseBodyOrStatus
		fun setUA(ua: String)
	}
}
