package net.e621.e6l.jvm

import net.e621.e6l.Platform.WebClient
import net.e621.e6l.Platform.WebClient.Companion.ResponseBodyOrStatus
import net.e621.e6l.Platform.WebClient.Companion.paramsToQueryString
import okhttp3.*
import okhttp3.internal.Version

object JVMActualWebClient: WebClient {
	//TODO log returned status codes
	private object UserAgentInterceptor: Interceptor {
		override fun intercept(chain: Interceptor.Chain): Response =
			chain.proceed(chain.request().newBuilder().header("User-Agent", userAgent).build())
	}

	private val INSTANCE by lazy { OkHttpClient.Builder().addNetworkInterceptor(UserAgentInterceptor).build() }
	lateinit var userAgent: String

	override fun genUAFragment(): String = Version.userAgent()
	override fun get(uri: String, params: Map<String, Any>?): ResponseBodyOrStatus =
		INSTANCE.newCall(Request.Builder().url(uri + paramsToQueryString(params)).build()).execute().run {
			body()?.let { ResponseBodyOrStatus(code(), it.string()) } ?: ResponseBodyOrStatus(code())
		}
	/**
	 * [tutorial](http://square.github.io/okhttp)
	 */
	override fun post(uri: String, params: Map<String, Any>?): ResponseBodyOrStatus = TODO()
	override fun setUA(ua: String) { userAgent = ua }
}
