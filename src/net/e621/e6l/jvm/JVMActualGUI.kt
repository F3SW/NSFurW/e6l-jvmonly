package net.e621.e6l.jvm

import net.e621.e6l.Main.setImplementations
import net.e621.e6l.Platform.GUILibrary
import tornadofx.App
import tornadofx.launch

object JVMActualGUI: GUILibrary {
	/**
	 * TornadoFX, while certainly an improvement over using JavaFX directly, is very poorly documented. Very few
	 * properties and functions have Dokka comments and the guide "book" that replaced the GitHub wiki glosses over most
	 * uses of each control and flow element. In the future, this GUILibrary implementation should be replaced with
	 * [this Kotlin port of a C++ library](https://github.com/kotlin-graphics/imgui).
	 */
	class E6lTornadoFXApp: App(E6lMasterView::class) {
		//TODO add global app icon, tried:
//		override fun start(stage: Stage) {
//			super.start(stage)
//			addStageIcon(Image(resources["icon.png"]))
//		}

		//TODO get focus on launch, tried:
//		override fun onBeforeShow(view: UIComponent) = view.currentStage?.requestFocus() ?: Unit
//		override fun onBeforeShow(view: UIComponent) = view.primaryStage.requestFocus()
//		override fun onBeforeShow(view: UIComponent) = view.currentWindow?.requestFocus() ?: Unit
	}
	private const val COMPILE_TARGET_NAME = "JVM"
	override fun initAndDrawMain(args: Array<String>) {
		setImplementations(COMPILE_TARGET_NAME, JVMActualFS, JVMActualGUI, JVMActualJSON, JVMActualWebClient)
		launch<E6lTornadoFXApp>(args)
	}
}
