package net.e621.e6l.jvm.e6_tabbed

import tornadofx.*

class CommentsTabView: View() {
	override val root = vbox {
		hbox {
			button("List")
			button("Search")
			button("Comments on Your Posts")
			button("Help")
		}
		hbox {
			label("TODO Comments")
		}
	}
}
