package net.e621.e6l.jvm.e6_tabbed

import tornadofx.*

class WikiTabView: View() {
	override val root = vbox {
		hbox {
			button("List")
			button("New")
			button("Help")
			hbox {
				label("|")
				button("Posts")
				button("History")
				add(WikiUnlockedPageShortcutView::class)
			}
		}
		hbox {
			label("TODO Wiki")
		}
	}
}

class WikiLockedPageShortcutView: View() {
	override val root = hbox {
		button("Page locked")
	}
}

class WikiUnlockedPageShortcutView: View() {
	override val root = hbox {
		button("Edit")
		button("Report")
	}
}
