package net.e621.e6l.jvm.e6_tabbed

import tornadofx.*

class ForumTabView: View() {
	override val root = vbox {
		hbox {
			button("List")
			button("Categories")
			button("New Topic")
			button("Help")
			label("|")
			button("Mark all read")
		}
		hbox {
			label("TODO Forum")
		}
	}
}
