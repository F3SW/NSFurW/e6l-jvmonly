package net.e621.e6l.jvm.e6_tabbed

import tornadofx.*

class PoolsTabView: View() {
	override val root = vbox {
		hbox {
			button("List")
			button("New")
			button("Recent change")
			button("Help")
		}
		hbox {
			label("TODO Pools")
		}
	}
}
