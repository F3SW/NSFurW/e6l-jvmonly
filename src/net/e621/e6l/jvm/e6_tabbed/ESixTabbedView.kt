package net.e621.e6l.jvm.e6_tabbed

import javafx.scene.Node
import javafx.scene.control.TabPane.TabClosingPolicy
import net.e621.e6l.ESixAPI
import tornadofx.*
import kotlin.reflect.KClass

class ESixTabbedController: Controller() {
	companion object {
		private const val GET_FAILED_MSG = "oops"
	}
	fun writeToDb(inputValue: String): String {
		var x = 0
		for (i in 0..Int.MAX_VALUE) x = i
		println("Writing $inputValue to database!")
		return "written!"
	}
	fun tagList() = ESixAPI.Tags.list("*test*")?.firstOrNull()?.id?.toString() ?: GET_FAILED_MSG
	fun tagList1() = ESixAPI.Tags.list1(13371)?.id?.toString() ?: GET_FAILED_MSG
	fun postList() = ESixAPI.Posts.list("rating:s", limit = 10)?.firstOrNull()?.tags ?: GET_FAILED_MSG
	fun postList1(id: Int) = ESixAPI.Posts.list1(id)?.tags ?: GET_FAILED_MSG
}

class ESixTabbedView: View() {
	private lateinit var mainViewRoot: Node
	private fun <T: UIComponent> replaceCenter(kClass: KClass<T>) {
		//TODO dunno what the DSL's syntax is
//		root.center = kClass by inject()
	}
	override val root = borderpane {
		top {
			tabpane {
				tabClosingPolicy = TabClosingPolicy.UNAVAILABLE
				tab("Home") {
					add(HomeTabView::class)
				}.select()
				tab("Posts") {
					add(PostsTabView::class)
				}
				tab("Comments") {
					add(CommentsTabView::class)
				}
				tab("Notes") {
					add(NotesTabView::class)
				}
				tab("Artists") {
					add(ArtistsTabView::class)
				}
				tab("Tags") {
					add(TagsTabView::class)
				}
				tab("Pools") {
					add(PoolsTabView::class)
				}
				tab("Sets") {
					add(SetsTabView::class)
				}
				tab("Blips") {
					add(BlipsTabView::class)
				}
				tab("Wiki") {
					add(WikiTabView::class)
				}
				tab("Forum") {
					add(ForumTabView::class)
				}
				tab("Misc.") {
					add(MiscTabView::class)
				}
			}
		}
	}
}
