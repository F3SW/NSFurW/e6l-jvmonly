package net.e621.e6l.jvm.e6_tabbed

import tornadofx.*

class NotesTabView: View() {
	override val root = vbox {
		hbox {
			button("List")
			button("Search")
			button("History")
			button("Requests")
			button("Help")
		}
		hbox {
			label("TODO Notes")
		}
	}
}
