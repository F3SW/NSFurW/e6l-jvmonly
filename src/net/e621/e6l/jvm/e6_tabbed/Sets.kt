package net.e621.e6l.jvm.e6_tabbed

import tornadofx.*

class SetsTabView: View() {
	override val root = vbox {
		hbox {
			button("List")
			button("New")
			button("Help")
			button("My Sets")
			button("My Invites")
		}
		hbox {
			label("TODO Sets")
		}
	}
}
