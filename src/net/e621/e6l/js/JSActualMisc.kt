package net.e621.e6l.js

import net.e621.e6l.Platform.JSONImplementation
import net.e621.e6l.Platform.WebClient

object JSActualJSON: JSONImplementation {
	override fun parseDataModifiedResponse(jsonString: String) = TODO()
	override fun parseFFDList(jsonString: String) = TODO()
	override fun parseInternalPost(jsonString: String) = TODO()
	override fun parseInternalPostList(jsonString: String) = TODO()
	override fun parseInternalTag(jsonString: String) = TODO()
	override fun parseInternalTagAlias(jsonString: String) = TODO()
	override fun parseInternalTagList(jsonString: String) = TODO()
	override fun parsePostExistanceStatus(jsonString: String) = TODO()
	override fun parsePostUploadResponse(jsonString: String) = TODO()
	override fun parseWikiPage(jsonString: String) = TODO()
}

object JSActualWebClient: WebClient {
	lateinit var userAgent: String
	override fun genUAFragment() = TODO()
	override fun get(uri: String, params: Map<String, Any>?) = TODO()
	override fun post(uri: String, params: Map<String, Any>?) = TODO()
	override fun setUA(ua: String) { userAgent = ua }
}
