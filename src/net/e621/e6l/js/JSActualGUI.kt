package net.e621.e6l.js

import net.e621.e6l.Main.setImplementations
import net.e621.e6l.Platform.GUILibrary

object JSActualGUI: GUILibrary {
	private const val COMPILE_TARGET_NAME = "JS"
	override fun initAndDrawMain(args: Array<String>) {
		setImplementations(COMPILE_TARGET_NAME, JSActualFS, JSActualGUI, JSActualJSON, JSActualWebClient)
		TODO()
	}
}
